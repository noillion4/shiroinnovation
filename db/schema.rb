# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_26_191037) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "alikes", id: :serial, force: :cascade do |t|
    t.integer "aid"
    t.integer "userid"
  end

  create_table "amis", id: :serial, force: :cascade do |t|
    t.integer "u1"
    t.integer "u2"
  end

  create_table "animers", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "content"
    t.text "body"
    t.string "image_uid"
    t.string "auteur"
    t.string "source"
    t.string "licencier"
    t.datetime "datetime"
    t.integer "nlike"
    t.integer "valider"
  end

  create_table "categorieas", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "tilte"
  end

  create_table "categorieforms", id: :serial, force: :cascade do |t|
    t.integer "nbmessage"
    t.string "name"
    t.datetime "dateup"
    t.string "image_uid"
  end

  create_table "categoriejeuxes", id: :serial, force: :cascade do |t|
    t.string "name"
  end

  create_table "categoriejeuxs", id: :serial, force: :cascade do |t|
    t.string "name"
  end

  create_table "categorieseries", id: :serial, force: :cascade do |t|
    t.string "name"
  end

  create_table "chats", id: :serial, force: :cascade do |t|
    t.string "identifier"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "commentaireanimes", id: :serial, force: :cascade do |t|
    t.string "content"
    t.integer "typ"
    t.string "auteur"
    t.string "image_uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "idt"
  end

  create_table "commentairejeuxes", id: :serial, force: :cascade do |t|
    t.string "content"
    t.integer "typ"
    t.string "auteur"
    t.string "image_uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "idt"
  end

  create_table "commentairejeuxs", id: :serial, force: :cascade do |t|
    t.string "content"
    t.integer "typ"
    t.string "auteur"
    t.string "image_uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "idt"
  end

  create_table "commentaires", id: :serial, force: :cascade do |t|
    t.string "content"
    t.integer "typ"
    t.string "auteur"
    t.string "image_uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "idt"
  end

  create_table "commentaireseries", id: :serial, force: :cascade do |t|
    t.string "content"
    t.integer "typ"
    t.string "auteur"
    t.string "image_uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "idt"
  end

  create_table "conversations", id: :serial, force: :cascade do |t|
    t.integer "recipient_id"
    t.integer "sender_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["recipient_id", "sender_id"], name: "index_conversations_on_recipient_id_and_sender_id", unique: true
    t.index ["recipient_id"], name: "index_conversations_on_recipient_id"
    t.index ["sender_id"], name: "index_conversations_on_sender_id"
  end

  create_table "demandes", id: :serial, force: :cascade do |t|
    t.integer "ud"
    t.integer "ur"
  end

  create_table "dlikes", id: :serial, force: :cascade do |t|
    t.integer "iduser"
    t.integer "idpub"
  end

  create_table "followers", force: :cascade do |t|
    t.integer "u1"
    t.integer "u2"
  end

  create_table "jeuxes", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "content"
    t.datetime "datetime"
    t.string "image_uid"
    t.string "auteur"
    t.string "source"
    t.string "licencier"
    t.integer "nlike"
    t.integer "valider"
  end

  create_table "jeuxgenres", id: :serial, force: :cascade do |t|
    t.string "categoriejeux_id"
    t.string "jeux_id"
  end

  create_table "jeuxs", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "content"
    t.datetime "datetime"
    t.string "image_uid"
    t.string "auteur"
    t.string "source"
    t.string "licencier"
  end

  create_table "jlikes", id: :serial, force: :cascade do |t|
    t.integer "userid"
    t.integer "jid"
  end

  create_table "listegenres", id: :serial, force: :cascade do |t|
    t.string "categoriea_id"
    t.string "animer_id"
  end

  create_table "listgenres", id: :serial, force: :cascade do |t|
    t.string "anime"
  end

  create_table "messages", id: :serial, force: :cascade do |t|
    t.text "body"
    t.integer "user_id"
    t.integer "conversation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "vue"
    t.index ["conversation_id"], name: "index_messages_on_conversation_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "photos", id: :serial, force: :cascade do |t|
    t.string "image_uid"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user"
  end

  create_table "posts", id: :serial, force: :cascade do |t|
    t.text "contenu"
    t.datetime "date"
    t.integer "user_id"
    t.integer "souscatforms_id"
    t.string "image_uid"
    t.string "content"
  end

  create_table "seriegenres", id: :serial, force: :cascade do |t|
    t.string "categorieserie_id"
    t.string "serie_id"
  end

  create_table "series", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "content"
    t.string "image_uid"
    t.string "auteur"
    t.string "source"
    t.string "licencier"
    t.datetime "datetime"
    t.integer "nlike"
    t.integer "valider"
  end

  create_table "setdbdatas", force: :cascade do |t|
    t.integer "userid"
    t.string "style"
    t.string "value"
  end

  create_table "slikes", id: :serial, force: :cascade do |t|
    t.integer "userid"
    t.integer "sid"
  end

  create_table "souscatforms", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "user_id"
    t.integer "derniermessage"
    t.integer "categorieform_id"
    t.integer "nbmessage"
  end

  create_table "subscriptions", id: :serial, force: :cascade do |t|
    t.integer "chat_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tweets", id: :serial, force: :cascade do |t|
    t.string "auteur"
    t.string "tilte"
    t.integer "aime"
    t.integer "ido"
    t.string "type"
    t.integer "typ"
    t.string "image_uid"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.string "content"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "salt"
    t.string "name"
    t.string "emal"
    t.string "password"
    t.string "manga"
    t.string "jeux"
    t.string "animer"
    t.string "serie"
    t.string "film"
    t.string "pays"
    t.string "presentation"
    t.string "sexe"
    t.integer "nbmnl"
    t.integer "role"
    t.string "image_uid"
    t.string "pseudo"
    t.datetime "lasttime"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
