Rails.application.routes.draw do
  scope "(:locale)", :locale => /en|fr/ do


  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'forum/index'

  get 'forum/show'

  get 'forum/sujet'
	post '/anime/aime/:id' => 'geek#aime'
post '/jeux/aime/:id' => 'jeux#aime'
post '/serie/aime/:id' => 'serie#aime'
  root 'tweets#index'
  get '/sujet/:id'=>'forum#sujet'
    get '/freche'=>'forum#reche'
      post '/freche'=>'forum#reche'
  get 'geek/animer'
  get 'forumshow/:id'=>'forum#show'
  get 'forum' =>'forum#index'
  post 'addsouscat'=>'forum#addsouscat'
    post 'createpost'=>'forum#createpost'
  resources :animers
post 'serie/createcom'
post 'geek/createcom'
post 'jeux/createcom'
  get 'users/user'
  get 'users/log'
  get 'addanime'=>'geek#addanime'
    get 'anime/:id'=>'geek#unanime'
    get 'arendom'=>'geek#aleatoirea'
      get 'arechegenre'=>'geek#arechegenre'
      post 'arechegenre'=>'geek#arechegenre'
  post 'save'=>'geek#save'
  patch 'update' =>'users#up'
get 'chats' =>'home#index'
get 'addmessage'=>'tweets#addmessage'
post 'mesot'=>'tweets#meso'
#route pour les jeux
post 'jcreate' =>'jeux#jcreate'
get 'addjeux' =>'jeux#addjeux'
get 'lesjeux' =>'jeux#lesjeux'
get 'jeuxt' =>'jeux#jeux'
get 'jeux/:id'=>'jeux#unjeux'
get 'aleatoirej'=>'jeux#aleatoirej'
  get 'jrechegenre'=>'jeux#jrechegenre'
   post 'jrechegenre'=>'jeux#jrechegenre'
post '/jreche' =>'jeux#reche'
#route pour les serie
post 'screate' =>'serie#screate'
get 'addserie' =>'serie#addserie'
get 'lesseries' =>'serie#lesseries'
get 'seriet' =>'serie#serie'
get 'serie/:id'=>'serie#uneserie'
get 'aleatoires'=>'serie#aleatoires'
  get 'srechegenre'=>'serie#srechegenre'
   post 'srechegenre'=>'serie#srechegenre'
post '/sreche' =>'serie#reche'
#
post 'users/connection' => 'users#connection'
  get 'users/log' => 'users#log'
    get 'users/amis' => 'users#amis'
      get 'users/profil/:id/:pt'=> 'users#profil'
post' animers_path' =>'users#animers_path'
get 'users/deco' => 'users#deco'
resources :photos, only: [:new, :create, :index]
get 'show/:id' => 'tweets#show'
get 'tweeta' => 'tweets#tweeta'
get 'follow' => 'tweets#follow'
get '/tweets/convers' => 'tweets#convers'

get '/setvaluedb' => 'tweets#setvaluedb'
get '/optmessage' => 'tweets#optmessage'
  post 'users/tweet' => 'users#login'
post '/followers/:id' => 'tweets#followers'
post 'tweets/createa' => 'tweets#createa'
post 'createcom' => 'tweets#createcom'
post 'aime/:id' => "tweets#aime"
post 'aime2/:id' => "tweets#aime2"
post '/updat' =>'users#updat'
post '/reche' =>'users#reche'
get '/reche' =>'users#reche'
post '/areche' =>'geek#reche'
get '/lesanimes' =>'geek#lesanimes'
get '/chat' =>'users#chat'
get '/show' =>'users#show'
post '/demande/:id' =>'users#demande'
post '/acc/:id' =>'users#accept'
post '/sup/:id' =>'users#supr'
end
resources :tweets

  devise_for :users

  resources :conversations, only: [:create] do
    member do
      post :close
    end

    resources :messages, only: [:create]
  end

end
