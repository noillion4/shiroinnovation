class CreateCommentaire < ActiveRecord::Migration[5.0]
  def change
    create_table :commentaires do |t|
      t.string :content
      t.integer :typ
      t.string :auteur
      t.string :image_uid
      t.timestamps
    end
	
  end
end
