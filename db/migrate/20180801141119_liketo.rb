class Liketo < ActiveRecord::Migration[5.0]
  def change
	create_table  :slikes
	add_column :slikes , :userid , :int
	add_column :slikes , :sid , :int 
	create_table :jlikes 
	add_column :jlikes , :userid , :int	
	add_column :jlikes , :jid , :int
	add_column :jeuxes , :nlike , :int
	add_column :series , :nlike , :int 

  end
end
class ConvertDatabaseToUtf8mb4 < ActiveRecord::Migration[5.0]
  def change
    # for each table that will store unicode execute:
    execute "ALTER TABLE tweets CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin"
    # for each string/text column with unicode content execute:
    execute "ALTER TABLE tweets CHANGE content VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin"
  end
end
