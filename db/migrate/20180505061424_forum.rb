class Forum < ActiveRecord::Migration[5.0]
  def change
	create_table :categorieforms
	add_column :categorieforms, :nbmessage , :integer
	add_column :categorieforms , :name , :string
	add_column :categorieforms, :dateup , :datetime

		
	create_table :souscatforms
	add_column :souscatforms , :name , :string 
	add_column :souscatforms , :user_id , :integer
	add_column :souscatforms, :derniermessage , :integer 
	add_column :souscatforms, :categorieform_id, :integer
	add_column :souscatforms, :nbmessage, :integer


	create_table :posts
	add_column :posts , :contenu , :text 
	add_column :posts , :date , :datetime
	add_column :posts , :user_id , :integer
	add_column :posts , :souscatforms_id , :integer
  end
end
