class PhotosController < ApplicationController
  def index
    @photos = Photo.all
  end

  def new

  end

  def create
    @photo = Photo.new(photo_params)
    @photo.user = current_user.id
    if @photo.save
      flash[:success] = "Photo saved!"

      redirect_to "/users/user"
    else
      render 'new'
    end
  end

  private

  def photo_params
    params.require(:photo).permit(:image, :title)
  end

end
